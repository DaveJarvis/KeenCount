# KeenCount

A Java library that counts the number of occurrences for each word in
a document.

# Build

Build and compress the library as follows:

``` bash
./gradlew clean build squish
```

The library is built and optimized.

