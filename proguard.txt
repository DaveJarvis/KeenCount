# ProGuard cannot resolve MethodHandle.invokeExact
-dontwarn 'java.lang.invoke.MethodHandle'
-dontwarn 'java.lang.invoke.VarHandle'

# Ensure reflection doesn't break
-dontobfuscate

-keep class org.apache.lucene.analysis.tokenattributes.KeywordAttribute
-keep class org.apache.lucene.analysis.tokenattributes.KeywordAttributeImpl
-keep public class com.whitemagicsoftware.** { *; }
