#!/usr/bin/env bash

MODULES="${JAVA_HOME}/jmods/"

java -jar lib/proguard.jar \
  -libraryjars "${MODULES}java.base.jmod/(!**.jar;!module-info.class)" \
  -libraryjars "${MODULES}java.desktop.jmod/(!**.jar;!module-info.class)" \
  -libraryjars "${MODULES}java.xml.jmod/(!**.jar;!module-info.class)" \
  -libraryjars "${MODULES}java.logging.jmod/(!**.jar;!module-info.class)" \
  -injars build/libs/keencount.jar \
  -outjars build/libs/keencount-min.jar \
  -keep 'class com.whitemagicsoftware.** { *; }' \
  -keep 'class com.ibm.icu.** { *; }' \
  -dontnote \
  -dontwarn \
  -dontoptimize \
  -dontobfuscate

