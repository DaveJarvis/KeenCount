/* Copyright 2023 White Magic Software, Ltd. -- All rights reserved.
 *
 * SPDX-License-Identifier: MIT
 */
package com.whitemagicsoftware.keencount;

import java.util.Locale;

/**
 * Responsible for creating an instance of {@link Tokenizer} for a language.
 */
public class TokenizerFactory {
  /**
   * Clients must use the {@link #create(Locale)} method.
   */
  private TokenizerFactory() { }

  /**
   * Creates a new {@link Tokenizer} instance for the given language.
   *
   * @param locale The locale-based language to give context for tokenizing.
   * @return The {@link Tokenizer} that can segment words for a language.
   * @throws TokenizerException Could not instantiate the third-party library
   *                            responsible for text tokenization.
   */
  public static Tokenizer create( final Locale locale )
    throws TokenizerException {
    try {
      return new Icu4jTokenizer( locale );
    } catch( final Exception ex ) {
      throw new TokenizerException( ex );
    }
  }
}
