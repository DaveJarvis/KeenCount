/* Copyright 2023 White Magic Software, Ltd. -- All rights reserved.
 *
 * SPDX-License-Identifier: MIT
 */
package com.whitemagicsoftware.keencount;

import com.ibm.icu.text.BreakIterator;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import static com.ibm.icu.text.BreakIterator.WORD_NONE;
import static com.ibm.icu.text.BreakIterator.getWordInstance;

/**
 * Provides an implementation of {@link Tokenizer} that can split a document
 * into individual terms. Each term is tallied and returned.
 */
public class Icu4jTokenizer implements Tokenizer {

  private final BreakIterator mIterator;

  public Icu4jTokenizer( final Locale locale ) {
    assert locale != null;

    mIterator = getWordInstance( locale );
  }

  @Override
  public Map<String, int[]> tokenize( final String document )
    throws TokenizerException {
    assert document != null;

    final var result = new HashMap<String, int[]>();
    final var text = document.replace( "&apos;", "'" );

    mIterator.setText( text );

    int began = mIterator.first();

    for( int ended = mIterator.next();
         ended != BreakIterator.DONE;
         began = ended, ended = mIterator.next() ) {
      final var word = text.substring( began, ended ).toLowerCase();

      if( mIterator.getRuleStatus() != WORD_NONE ) {
        result.computeIfAbsent( word, k -> new int[]{0} )[ 0 ]++;
      }
    }

    return result;
  }
}
